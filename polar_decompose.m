function [U,P] = polar_decompose(A,it)
%A = U*P

U = A;
for i =1:it
    U = 0.5*(U+inv(adjugate(U)));
end
P = inv(U)*A;

end