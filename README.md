Viele Beispiele benötigen eigene Matlab Klassen. Am besten laden sie das gesamte Matlab-Script-Verzeichnis herunter. 

[Matlab-Scripts als ZIP downloaden](https://gitlab.com/tscheims1/matlab-stuff/repository/archive.zip?ref=master)

## Grundsätzliches

In diesem Kurs habe ich einige Matlab (Hilfs)klassen erstellt.

[**HomogenousFigure**](HomogenousFigure.m)
Eine Klasse, welche für Rotation, Skalierung, Scherung, Translation, Spiegelung und perspektivische Darstellung von 2D Körpern im homogenen Raum.

[**HomogenousFigure3**](HomogenousFigure3.m)
Eine Klasse, welche für Rotation, Skalierung, Scherung, Translation, Spiegelung und perspektivische Darstellung von 3D Körpern im homogenen Raum.

[**PlotHelper**](PlotHelper.m)
Eine Hilfsklasse zum Plotten (z.B. generiert Cubes, Pyramiden, zeichet Basisachsen, ect)

[**Quaternion**](Quaternion.m)
Diese Klasse repräsentiert ein Quaternion. Die gängisten Quaternion-Operationen sind implementiert

[**DualQuaternion**](DualQuaternion.m)
Diese Klasse repräsentiert ein duales Quaternion. Die Klasse benötigt die Klasse Quaternion. Die gängisten DualQuaternion-Operationen sind implementiert



## Affine Transformationen in R^2 

Eine affine Transformation in 2D sieht wie folgt aus:

![Affin Eq](img/affin-eq-general.png)


### Rotation in R^2

Eine Rotationsmatrix in R^2  
![Rot in 2D](img/rot2d.png)

Wobei θ den Rotationswinkel beschreibt.

### Skalierung in R^2

![skalierung in R2](img/scalexy.png)
Sind beide Werte identisch, handelt es sich um eine uniforme Skalierung (d.h. die Ähnlichkeit bleibt erhalten)


### Scherung in x-Richtung in R^2 

![Scherung in R2](img/shear-x.png)

### Scherung in y-Richtung in R^2 

![Scherung in R2](img/shear-y.png)

### Spiegelung an X-Achse in R^2 

![reflext x](img/refelct_x-mat.png)

### Spiegelung an Y-Achse in R^2

![reflect y ](img/reflect_y-mat.png)


**Beispiel:**


Spiegelung an einer Geraden, Rotation um einen Punkt, Scherung, Skalierung in R^2 (Matlab Plot)

![div. Transformation in R^2](img/2D-affin-rot-scale-reflect-shear.png)

[affine Transformationen in R^2 (Matlab Script)](img/A1_affin.m)

## Homogene Transformationen in P^2

In P^2 kann die Translation als lineare Operation beschrieben werden.
Zusätzlich können die Fluchtpunkte in X- und Y-Richtung bestimmt werden.
![homogene transformation](img/homogen-eq-general.png)


Beispiel: Rotation θ um den Punkt t
![matrix stack](img/homo-matrix-stack.png)


**Beispiel**

Rotation und Spiegelung an Geraden in P^2 (Matlab Plot)
![Transform homogen](img/2D-RotReflect.png)


Fluchtpunkt liegt im endlichen (Matlab Plot)
![2D Perspective](img/2d-perspective.png)


[Homogene Transformationen in P^2 (Matlab-Script)](A1.m)

[Beispiel Fluchtpunkte nicht unendlich (Matlab-Script)](A1_perspective.m)

[Klasse HomogenousFigure für Berechnungen in P^2 (Matlab-Script)](HomogenousFigure.m)


**Beispiel Matlab UI Programm zum Manipulieren eines Quadrats**


![UI Matlab 2d](img/matlab-ui-example.png)
[Source Code der UI-Anwendung (Matlab-Script)](ui3.m)

## Rotation in C

Mit den Komplexen Zahlen lässt sich in 2D eine Rotation beschreiben.
Die Koordinaten sind dabei als Komplexe Zahl zu interpretieren.

Eine Rotation ist in C wie folgt zu repräsentieren:
![Rotation in C](img/complex2d.png)


**Beispiel**

![Rotation um Ursprung Komplex](img/2d-rot-complex.png)
[Rotation in C Beispiel (Matlab Script)](A1_complex.m)


## Transformation in R^3

### Skalierung in R^3

![Skalierung in R^3](img/scalexyz.png)
Sind alle drei Werte identisch, handelt es sich um eine uniforme Skalierung (d.h. die Ähnlichkeit bleibt erhalten)

### Scherung in R^3


Die Matrizen für die Scherungen sehen wie folgt aus:

**XY-Richtung**

![scherung xy richtung](img/shear-xy.png)

**YZ-Richtung**

![scherung yz richtung](img/shear-yz.png)

**XZ-Richtung**

![scherung xz richtung](img/shear-xz.png)


**Beispiel: Scherung und Scalierung in R^3**
Bei diesem Beispiel wurde die Skalierung und Scherung im Zetrum ausgeführt. (Die Original-Pyramide bei der Scherung ist nicht sichtbar, weil sie sich unter der Skalierten Pyramide befindet)


![Scherung und Skalierung in R^3](img/3d-shear-scale.png)

[Klasse für Berechnungen aller Operationen in P^3](HomogenousFigure3.m)
[Scherung und Skalierung in R^3 (Matlab-Script)](A2_Shear_Scale.m)



### Spiegelung / Projektion in R^3

Die Matrix für die Spiegelung an einer Ebene, welche einen Punkt im Ursprung hat, sieht wie folgt aus:
![Matrix Spiegelung an Ebene 3D](img/3d-reflect-plane.png)
Wobei **v** der normierte Normalenvektor der zu reflektierenden Ebene ist.

Die Matrix für die Projektion auf eine Ebene sieht wie folgt aus:


![Matrix Projektion auf Ebene](img/3d-projection-onto-plane.png)

Wobei R eine Matrix aus zwei Eckpunkten der Ebene ist (als Ortsvektor repräsentiert)
![R Projektion auf Ebene](img/3d-projection-onto-plane0.png)

Falls die Ebene keinen Eckpunkt im Ursprung hat, gilt dasselbe wie bei der Rotation: 
 1. Ebene in den Ursprung verschieben
 2. Spiegelung / Projektion durchführen
 3. Ebene wieder zurück verschieben
 
Beispiel Projektion auf eine Ebene (mit homogenen Koordinaten, T ist eine reine Translationsmatrix)
![Projektion auf eine Ebene, welche nicht im Ursprung liegt](img/3d-projection-onto-plane2.png)

**Beispiel: Spiegelung an einer Ebene (welche nicht durch den Ursprung geht) / Projektion auf diese Ebene**

![3D Reflect Plane / Projection onto Plane](img/3d-reflect-plane-projection-plane.png)

[Spiegelung an Ebene / Projektion auf Ebene (Matlab-Script)](A2_Reflect_Plane.m)

[Klasse für Berechnungen aller Operationen in P^3](HomogenousFigure3.m)


### Rotation in R^3

Die Roation in R^3 ist wesentlich komplexer als die Rotation R^2. Während in 2D die Rotation nur einen Freiheitsgrad besietzt, besitzt die Rotation 3D 3 Freiheitsgrade.

**Rotation mit Euler-Angles**

Die folgenden Matrixen beschreiben eine Rotation um die jeweilige Achse:

![Rotation x-Axis](img/rot_x.png)

![Rotation y-Axis](img/rot_y.png)

![Rotation z-Axis](img/rot_z.png)

**Beispiel einer Rotation in R^3**
![Rotation in R^3](img/3d-rot-quaternion.png)

Damit eine Roation um eine bestimmte Achse durchgeführt werden kann, muss zuerst die Rotationsachse auf eine Basisachse rotiert, die Rotation durchgeführt und dann wieder
zurück rotiert werden.

Eine Mögliche Abfolge einer Rotation würde dann so aussehen:
![Rotation Stack 3d](img/rotstack-3d.png)

Wenn eine Rotation um einen Rotationvektor beschrieben werden soll, müssen bei dieser Vorgehensweise zuerst die Rotationswinkel der drei Rotationsmatrixen ermittelt werden.

**Beispiel: Rotation um eine Gerade mit Euler Angles**

[Rotation mit Euler Angles (Matlab-Script)](A2_Rot.m)

[Klasse für Berechnungen aller Operationen in P^3](HomogenousFigure3.m)


**Rotation mit Rodriguez Formel**

Wesentlich einfacher ist die Rotation mit der Rodriguez Formel.
Wobei **n** hier der normierte Rotationsvektor darstellt und θ den Rotationswinkel beschreibt.

![Rotationsmatrix Rodriguez](img/rod-mat.png)
![Rotformula Rodriguez](img/rod-formula.png)


**Beispiel: Rotation um eine Gerade mit Rodriguez Formel**

[Rotation mit Rodriguez (Matlab-Script)](A2_Rodrigues.m)

[Klasse für Berechnungen aller Operationen in P^3](HomogenousFigure3.m)

**Rotation mit Quaternionen**

Die Rotation in 3D kann auch mit Quaternionen durchgeführt werden.

Dazu sind folgende Schritte notwendig:
1. Rotationvektor normieren
2. Rotationsvektor als Quaternion darstellen
3. Der zu rotierende Punkt als Quaternion darstellen
4. Rotation mit Quaternionen durchführen (sog. Sandwich-Operation)

**Rotationsvektor als Quaternion darstellen**

Das Quaternion sieht wie folgt aus:
![Rotationsvektor als Quaternion](img/quaternion.png)
Wobei der Vektor **v** den Rotationsvektor darstellt und θ den Rotationswinkel beschreibt. 

**Der zu rotierende Punkt als Quaternion darstellen**

![Vektor as Quaternion](img/vector-as-quat.png)

**Rotation durchführen**

![Quaternion Rotation](img/sandwich-operation.png)


**Geometrische Darstellung der Quaternion Multiplikation**

![Geometrische Darstellung einer Quat Multiplikation](img/quaternion-geometrisch-rotation.png)
Achtung: Die Quaternion Multiplikation ist **nicht** kommutativ

**Beispiel Rotation um eine Gerade mit Quaternion**

![Quaternion Rotation in 3d](img/3d-rot-quaternion.png)

[Quaternion Rotation in 3D (Matlab-Script)](A2_Quaternion.m)

## Transformation in P^3

### Perspektive in P^3

**Beispiel: perspektivische Darstellung eines Würfels.**
![Perspektivische Darstellung eines Würfels](img/3d-perspective.png)

[Perspektivische Darstellung eines Wüfels (Matlab-Script)](A2_Perspective.m)

[Klasse für Berechnungen aller Operationen in P^3](HomogenousFigure3.m)

## Transformation in (H,H)

Jede Festkörpertransformation im 3D-Raum kann mit einer Translation und einer Rotation beschrieben werden.
Quaternionen haben dabei den Nachteil, dass nur eine Rotation im 3D-Raum beschrieben werden kann.

Um zusätzlich die Translation mit Quaternionen zu beschreiben, ist ein duales Quaternion zu verwenden.
Ein duales Quaternion ist ein geordnetes Paar von Quaternionen.


Die Operationen sind mit Quaternionen wie folgt zu definieren:

Wobei Q<sub>r</sub>,Q<sub>d</sub>,P<sub>r</sub>,P<sub>d</sub> Quaternionen sind

**Addition duale Quaternionen**

![Addition duale Quaternionen](img/dual-quat-add.png)

**Multiplikation duale Quaternionen**

![Mulitplikation duale Quaternionen](img/dual-quat-multi.png)


Um eine Festkörpertransformation in R^3 mit dualen Quaternionen durchzuführen, ist wie folgt vorzugehen:

1. Rotationsvektor normieren
2. Rotationsvektor als duales Quaternion darstellen
3. Translationsvektor als duales Quaternion darstellen
4. Der zu transferierende Punkt als duales Quaternion darstellen
5. Transformation mit dualen Quaternionen durchführen (sog. Sandwichoperation)


**Rotationsvektor als duales Quaternion darstellen**

![Rotationsvektor als Quaternion](img/quat-rort.png)

**Translationsvektor als duales Quaternion darstellen**

![Translationsvektor als Quaternion](img/quat-translate.png)

**der zu transformierende Punkt als duales Quaternion darstellen**

![Punkt als Quaternion](img/dualquat-point.png)

**Transformation mit dualen Quaternionen durchführen**

**Wichtig:** Die Länge des dualen Quaternions M sollte immer 1 sein, sonst wird zusätzlich eine Skalierung durchgeführt.

![Definition M](img/M-dual-quat.png)
![Definition M neg nonj](img/M-dual-quat-neg-conj.png)
![Sandwichoperation dual Quaternion](img/dualquat-sandwich.png)

der imaginäre Teil des 2. Quaternions des dualen Quaternions entspricht dem neuen Punkt.
![Neuer Punkt Dual Quaternion](img/dualquat-newpoint.png)




**Beispiel lineare Interpolation einer Translation um (2,1) und PI/2 mit Dualen Quaternionen**

![Lineare Interpolation Duale Quaternionen](img/2d-dual-quaternion-interpolation.png)

[Lineare Interpolation (Matlab-Script)](quaternion_lin_interpolV2.m)

## Projektarbeit Interpolation von Transformationen und Deformationen 

[Bericht downloaden](interpolation/Interpolation_Rigid_and_non_Rigid.pdf)

[Matlab-Scripts](/interpolation) 