%% Class for Homogenous 3D Cacluations

classdef HomogenousFigure3
   properties
     fig
   end
   methods
       %% Transform into Homogenous room
       function obj = HomogenousFigure3(fig)
        
           obj.fig = [fig;ones(1,length(fig))];
       
       end
       %% Rotate around a Vector with Euler Angles
       function r = Rotate(obj,rotVector,phi)
           
           rotVector2 = [rotVector(1,1),rotVector(1,2)];
           eXY = [1,0]; %projection onto xy plane
           e3 = [0,0,1];
           
           cAlpha = cos(phi);
           sAlpha = sin(phi);
           
           cBeta = dot(eXY,rotVector2);
           
           cBetaNorm = (norm(rotVector2));
           if cBetaNorm ~= 0
            cBeta = cBeta/cBetaNorm;
           end
           
           sBeta = sqrt(1-cBeta^2);
           
           sGamma = dot(e3,rotVector);
           sGammaNorm = norm(rotVector);
           
           if sGammaNorm ~= 0
            sGamma = sGamma /sGammaNorm;
           end
           cGamma = sqrt(1-sGamma^2);
           
            Rx = [1,0,0,0;
                  0, cAlpha,-sAlpha,0;
                  0,sAlpha,cAlpha,0;
                  0,0,0,1];
              
            Ry = [cGamma,0,sGamma,0;
                  0,1,0,0;
                  -sGamma,0,cGamma,0
                  0,0,0,1;];
              
              Rz = [cBeta,sBeta,0,0;
                    -sBeta,cBeta,0,0;
                    0,0,1,0;
                    0,0,0,1];
           
              
        r.fig = inv(Rz)*inv(Ry)*Rx*Ry*Rz*obj.fig;
       
       end
       %% Rotate with Rodrigues
       function r = RodriguesRot(obj,rotVector,phi)
           n = rotVector/norm(rotVector);
            N = [0,-n(1,3),n(1,2),0;...
                 n(1,3),0,-n(1,1),0;...
                 -n(1,2),n(1,1),0,0;...
                 0,0,0,1];
             
             RodMat = eye(4)+ N*sin(phi)+N^2*(1-cos(phi));
             r.fig = RodMat*obj.fig;
       end
       
       %% Reflect on Plane A*x+B*y+C*Z + D = 0
        function r = ReflectPlane3(obj,A,B,C,D)
           n = [A;B;C];
           n = n/norm(n);
           F = eye(3)-2*n*n';
           FH = [F,zeros(3,1);
                 0,0,0,1];
           
           T = [1,0,0,0;
                0,1,0,0;
                0,0,1,D/C;
                0,0,0,1];
           r.fig = inv(T)*FH*T*obj.fig;
           
        end
        %% Scale
        function r = Scale(obj,s,t)
            scaleM =  [diag(s),zeros(3,1);
                       0,0,0,1];
            tM = [eye(3),-t';
                  0,0,0,1];
                      
                 
            r.fig = inv(tM)*scaleM*tM*obj.fig;        
        end
        %% Shear in XY Direction
        function r = ShearXY(obj,x,y,t)
           ShearXYM = [1,0,x,0;
                       0,1,y,0;
                       0,0,1,0;
                       0,0,0,1]; 
                   
             tM = [eye(3),-t';
                  0,0,0,1];
            r.fig = inv(tM)*ShearXYM*tM*obj.fig;
            
        end
        %% Projection onto a Plane
        function r = ProjectOntoPlane(obj,P1,P2,P3)
            
           P1 = P1 - P3;
           P2 = P2 - P3;
           T = [eye(3),-P3';
                0,0,0,1];
            
            R = [P1'/norm(P1),P2'/norm(P2)];
            P = R*inv(R'*R)*R';
            HP = [P,zeros(3,1);
                  0,0,0,1];
            r.fig = inv(T)*HP*T*obj.fig;
        end
        function r = AddPerspective(obj,vPerspective)
            P = [eye(3),zeros(3,1);
                 1/vPerspective(1),1/vPerspective(2),1/vPerspective(3),1];
            r.fig = P*obj.fig; 
           
            %Dehomogenisierung
            r.fig = r.fig ./ [r.fig(4,:);r.fig(4,:);r.fig(4,:);r.fig(4,:)];
            
        end
        
   end
    
end
