clc; clear;
% Add Perspective in P^3

p = PlotHelper();
[vert,edges] = p.Cube3([0.5,0.5,0.5]);

h = HomogenousFigure3(vert');
vPerspective = [5,4,3];
h = h.AddPerspective(vPerspective);

%% Graphics
plot3(vPerspective(1),0,0,'ko');
hold on;
plot3(0,vPerspective(2),0,'ko');
plot3(0,0,vPerspective(3),'ko');

perspectiveLineX = [vPerspective(1),0,0;
                    h.fig(1,1),h.fig(2,1),h.fig(3,1)];
                
perspectiveLineY = [0,vPerspective(2),0;
                    h.fig(1,1),h.fig(2,1),h.fig(3,1)];
perspectiveLineZ = [0,0,vPerspective(3);
                    h.fig(1,1),h.fig(2,1),h.fig(3,1)];
plot3(perspectiveLineX(:,1),perspectiveLineX(:,2),perspectiveLineX(:,3));
plot3(perspectiveLineY(:,1),perspectiveLineY(:,2),perspectiveLineY(:,3));
plot3(perspectiveLineZ(:,1),perspectiveLineZ(:,2),perspectiveLineZ(:,3));

%static blue colormap
map = [0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3];
colormap(map);

h.fig(1,:)
trimesh(edges,h.fig(1,:),h.fig(2,:),h.fig(3,:));
axis ([-5,5,-5,5,-5,5]);

ax = gca;

ax.ZGrid = 'on';
ax.XGrid = 'on';
ax.YGrid = 'on';
ax.ZMinorGrid = 'on';
ax.XMinorGrid = 'on';
ax.YMinorGrid = 'on';

hold off;
