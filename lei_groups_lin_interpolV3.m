close all; clc; clear;
%% Linear interpolation


R = [10,0;
     0,10;];

I = eye(2); 
v = [0,1,1,0,0;
     0,0,1,1,0];
k = 6;
figure(1);
title('linear interpolation (scaling)');

for i=0:k
    t = i/k;
    interpol =   (1-t)*I+t*R;
    v2 = interpol *v;
    hold on;
    plot(v2(1,:),v2(2,:),'LineWidth',2);
    
end

axis equal;
axis tight;
hold off;

%% Exp Interpolation

figure(2);
title('exponential interpolation (scaling)');

S = R;
R = eye(2);

pold = [1,1]';
for i=0:k
    t = i/k;
    interpol = t*R*exp(t*log(S));
    v2 = interpol *v;
    hold on;
    plot(v2(1,:),v2(2,:),'LineWidth',2);

end

 
 axis equal;
 axis tight;
 hold off;
 
%% rotation

v = v+[1,1,1,1,1;0,0,0,0,0];

 
 
%% Inear Interpol rot

phi = pi/2;


R = [cos(phi),-sin(phi);
      sin(phi),cos(phi)];


figure(3);
title('linear interpolation (rotation)');
I = eye(2); 

k = 10;
tic;
for x=0:10000
for i=0:k
    t = i/k;
    interpol =   (1-t)*I+t*R;
    v2 = interpol *v;
   
end
end


toc
axis equal;
axis tight;
hold off;
 
 
%% AP interpolation rotation
figure(5);
title('Ap rotation');
phi = pi/2;


R = [cos(phi),-sin(phi);
      sin(phi),cos(phi)];
 
S = eye(2);
k = 10;
for i=0:k
    t = i/k;
    phiT = phi*t;
    R = [cos(phiT),-sin(phiT);
      sin(phiT),cos(phiT)];
    
    interpol = R*((1-t)*I+t*S);
    v2 = interpol *v;
    hold on;
    plot(v2(1,:),v2(2,:));

end

plot(v2(1,:),v2(2,:));
axis equal;
axis tight;
hold off;


%% Exp Interpolation rotation
figure(4);
title('exponential interpolation (rotation)');

phi = pi/2;


S = eye(2);

tic;
for x=0:10000
for i=0:k
    t = i/k;
    phiT = phi*t;
    R = [cos(phiT),-sin(phiT);
      sin(phiT),cos(phiT)];
    
    interpol = R*exp(t*log(S));
    v2 = interpol *v;
end
end
toc
plot(v2(1,:),v2(2,:));
axis equal;
axis tight;
hold off;



% plot rot Angles
t=0:0.01:1;
s=sin(2*pi*2*t);
c=cos(2*pi*2*t);
figure(3);
hold on
plot(s,c,'k');
hold off;
figure(4);
hold on;
plot(s,c,'k');
hold off;
figure(5);
hold on;
plot(s,c,'k');
hold off;
