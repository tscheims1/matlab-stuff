%% Class for Homogenous 2D Calculations

classdef HomogenousFigure
   properties
    fig 
   end
   methods
       %% Transform into Homogenous room
       function obj = HomogenousFigure(fig)
        obj.fig = [fig;ones(1,length(fig))];
       
       end
       %% Rotate around origin
       function r = Rotate(obj,phi)
        rotMat = [cos(phi),-sin(phi),0;
                  sin(phi),cos(phi),0;
                  0,0,1];
              
        r.fig = rotMat*obj.fig;
        
       end
       
       %% Rotate around fixed Point
       function r = RotateAroundPoint(obj,phi,x,y)
           
           transMat = [1,0,x;
                        0,1,y;
                        0,0,1];
           rotMat = [cos(phi),-sin(phi),0;
                  sin(phi),cos(phi),0;
                  0,0,1];
          
                      
           r.fig = transMat*rotMat*inv(transMat)*obj.fig;
       end
       
       %% Reflect on Line2
       function r = Reflect(obj,m,q)
            rotMat = [cos(atan(m)),-sin(atan(m)),0;
                  sin(atan(m)),cos(atan(m)),0;
                  0,0,1];
            transMat = [1,0,0;
                        0,1,q;
                        0,0,1;];
           reflextXMat = [1,0,0;
                       0,-1,0
                       0,0,1];
                  
           r.fig = transMat*rotMat*reflextXMat*inv(rotMat)*inv(transMat)*obj.fig;         
            
       end 
       %% Add a Perspective to the figure
       function r = AddPerspective(obj,x,y)
           persMat = [1,0,0;
                      0,1,0;
                      1/x,1/y,1];
           r.fig = persMat*obj.fig;
           
           %Dehomogenisierung
            r.fig = r.fig./[r.fig(3,:);r.fig(3,:);r.fig(3,:)];
       end

   end
    
end