
function main
global g l
g=9.81; l=1;

%Solution in time space
Y0=[0,6.25];                    % start velocity
t=linspace(0,9,1000);           % time
[t,XY]=ode45(@pendel,t,Y0);     % solving ODE

%solution in phase space
x=[-3:1/2:10];y=[-6:1/2:12];    % coordinates 
[a,b]=meshgrid(x,y);            % grid
Vx=b';Vy=-g/l*sin(a');          % ode field

%Graphics time space
figure(1); clf; hold on;     
plot(t,XY(:,1),'g','linewidth',2);
plot(t,XY(:,2),'b','linewidth',2);
grid on; hold off

%Graphics phase space
figure(2); clf; hold on;        % solutions in phase space
quiver(x,y,Vx',Vy','k');        % velocity field plot
for i=1:10                      % beginning with different start values
    y0=[0,1.045*i];
    [t,XY]=ode45(@pendel,t,y0);
    plot(XY(:,1),XY(:,2),'color','r','Linewidth',2);
end
axis('normal',[min(x),max(x),min(y),max(y)]);
hold off
end

%function differential equation
function dgl=pendel(t,x)
   global g l 
   dgl=[x(2); -g/l*sin(x(1))];
end