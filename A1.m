clc; clear;
% Rotate in P^2

figX = [1,2,3,3,2,1,1,2,3];
figY = [1,1,1,2,2,2,3,3,3];

%Identiy
E = [1,0;
     0,1;];

%rotation angle in radiant
phi = pi/4; 
 
%rotation centre
mCentre = [-4;-4];


%homogen translation vector
Mp = [E,mCentre;
      zeros(1,2),1]

R = [cos(phi),-sin(phi);
      sin(phi),cos(phi)];
  
%homogen rot matrix
Rp = [R,zeros(2,1);
      zeros(1,2),1]

%rotation angle in radiant
phi = pi/3;


%Transform Vector to homogenic coordinates
Ep = [figX;
      figY;
      ones(1,9)]
  
Fp = Mp*Rp*inv(Mp)*Ep;   
      
figX1 = Fp(1,:);
figY1 = Fp(2,:);

%rotate back to x-axis
figXY = [figX;figY;ones(1,9)];

m = 1.5;
q = 3;
lineX = -10:0.5:10;
lineY = m.*lineX+q;

qVector = [0;q];

rotLineM = [cos(atan(m)),-sin(atan(m)),0;
 sin(atan(m)),cos(atan(m)),0;
 0,0,1];

translateQ = [1,0,qVector(1,1);
              0,1,qVector(2,1);
              0,0,1];
          
translateNeg = [1,0,-qVector(1,1);
              0,1,-qVector(2,1);
              0,0,1];
          
          
reflectXAxis = [1,0,0;
                0,-1,0;
                0,0,1];
          

figReflectXY = translateQ*rotLineM*reflectXAxis*inv(rotLineM)*translateNeg*figXY;

plot(figX,figY,'ro')
hold on;

plot(mCentre(1,1),mCentre(2,1),'ko')

plot(linspace(10,-10,10),linspace(0,0,10),'k')
plot(linspace(0,0,10),linspace(10,-10,10),'k')

plot(lineX,lineY,'k')

h = HomogenousFigure([figX;figY]);

h.fig
h = h.Reflect(m,q)
plot(h.fig(1,:),h.fig(2,:),'ro')
plot(figX1,figY1,'bo')
plot(figReflectXY(1,:),figReflectXY(2,:),'go')

axis('square',[-8,8,-4,12]);
hold off;     
