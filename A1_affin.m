%% Linear and Affine Operations in R^2

%% Figure for example
figX = [1,2,3,3,2,1,1,2,3];
figY = [1,1,1,2,2,2,3,3,3];
figXY = [figX;figY];


%% Rotate around fixed point
%rot centre
mCentre = [linspace(-3,-3,9);
    linspace(-3,-3,9)];


%rotation angle in radiant
phi = pi/2;

% rotation matrix
R = [cos(phi),-sin(phi);
      sin(phi),cos(phi)];

affinFig = R*([figX;figY]-mCentre)+mCentre;
 

        

%% Scherung
scherung = [1,1.2;
            0,1];
t =  repmat([1;1],1,9);

%if the figure is not in the origin -> we have to translate the 
%figure in the origin first
ScherFig = (scherung*(figXY-t))+t;
 
        
        
%% Translate + Scale
scaleX = 2;
scaleY = 3;
%if the figure is not in the origin -> we have to translate the 
%figure in the origin first
scaleFig = ([scaleX,0;0,scaleY]*(figXY-t))+t;



%% Reflect onto Line Equation m*x +q = y 

%reflection matrix
reflectX = [1,0;
            0,-1];

% line EQ
m = 2;
q = 5;
lineX = -10:0.5:10;
lineY = m.*lineX+q;

%translation Vector of Line
qVector = [linspace(0,0,9);
            linspace(q,q,9)];

% rotation matrix
rotLineM = [cos(atan(m)),-sin(atan(m));
 sin(atan(m)),cos(atan(m))];


figReflectXY = (rotLineM*reflectX*inv(rotLineM)*(figXY-qVector))+qVector;

%% Graphics
plot(figX,figY,'ro')
hold on;

plot(mCentre(1,1),mCentre(2,1),'ko')

plot(lineX,lineY,'b')
plot(linspace(10,-10,10),linspace(0,0,10),'k')
plot(linspace(0,0,10),linspace(10,-10,10),'k')

plot(affinFig(1,:),affinFig(2,:),'bo')

plot(figReflectXY(1,:),figReflectXY(2,:),'go')

plot(ScherFig(1,:),ScherFig(2,:),'k*');

plot(scaleFig(1,:),scaleFig(2,:),'k+');

axis('square',[-12,8,-4,12]);
hold off;


  
      
