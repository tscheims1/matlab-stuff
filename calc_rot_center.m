%cotan = 1/tan
z = [0,0,1];

phi1 = pi/6;
t1 = [2,3];
M1 = [cos(phi1),-sin(phi1),t1(1);
      sin(phi1),cos(phi1),t1(2);
      0,0,1];

M2 = [cos(phi),-sin(phi),t1(1);
      sin(phi),cos(phi),t1(2);
      0,0,1];  
  

t1_3 = [t1,0];
rotCenter = 0.5*(t1_3+cross(z,t1_3/tan(phi1)));