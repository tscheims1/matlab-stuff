%% Rotation with Rodrigues
p = PlotHelper();
[vert,edges] = p.Pyramid3([0.7,0.7,0.7]);

rotVector = [1,1,0];
h = HomogenousFigure3(vert');
h = h.RodriguesRot(rotVector,pi/2);


%% Graphics
trimesh(edges,vert(:,1),vert(:,2),vert(:,3));

%static blue colormap
map = [0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3];
colormap(map);

hold on;
fig = h.fig';
trimesh(edges,fig(:,1),fig(:,2),fig(:,3));
p.PlotLine3(rotVector,10);
axis ([-3,3,-3,3,-3,3]);
axis equal;
ax = gca;

ax.ZGrid = 'on';
ax.XGrid = 'on';
ax.YGrid = 'on';
ax.ZMinorGrid = 'on';
ax.XMinorGrid = 'on';
ax.YMinorGrid = 'on';

hold off;
