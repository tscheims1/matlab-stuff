clc; clear;
% Rotate in P^2

figX = [1,2,3,3,2,1,1,2,3];
figY = [1,1,1,2,2,2,3,3,3];

%Identiy
E = [1,0;
     0,1;];

%rotation angle in radiant
phi = pi/4; 
 
%rotation centre
mCentre = [-4;-4];

%coordinates for the line eq: ax+by+c=0
%y = (-ax-c)/b
%lineX = -10:0.5:10;
%lineY = (-a.*lineX-c)./b;

%homogen translation vector
Mp = [E,mCentre;
      zeros(1,2),1]

R = [cos(phi),-sin(phi);
      sin(phi),cos(phi)];
  
%homogen rot matrix
Rp = [R,zeros(2,1);
      0,0,1]
  
vPointX = 3;
vPointY = 7;
  
PerspectiveM = [1,0,0;
                0,1,0;
                1/vPointX,1/vPointY,1];
                
                

%rotation angle in radiant
phi = pi/3;


%Transform Vector to homogenic coordinates
Ep = [figX;
      figY;
      ones(1,9)];
  
Fp = PerspectiveM*Ep;  
Fp = Fp ./[Fp(3,:);Fp(3,:);Fp(3,:)]
figX1 = Fp(1,:);
figY1 = Fp(2,:);

%rotate back to x-axis,
figXY = [figX;figY;ones(1,9)];


plot(figX,figY,'ro')
hold on;

%calculate vanish lines
vLineX1 = linspace(0,figX1(1,1));
vLinem = (vPointY-figY1(1,1))/(0-figX1(1,1));
vLineQ = linspace(vPointY,vPointY);
vLineY1 = vLinem.*vLineX1 +vLineQ;


vLinem2 = (0-figY1(1,1))/(vPointX-figX1(1,1));

vLineX2 = linspace(figX1(1,1),vPointX);

vLineQ2 = figY1(1,1)- figX1(1,1)*vLinem2; % calculate from the lin eq: y = m*x+q
vLineQ2 = linspace(vLineQ2,vLineQ2);
vLineY2 = vLinem2.* vLineX2+vLineQ2;

plot(vPointX,0,'ko')
plot(0,vPointY,'ko')
plot(linspace(10,-10,10),linspace(0,0,10),'k')
plot(linspace(0,0,10),linspace(10,-10,10),'k')
plot(vLineX1,vLineY1,'k')
plot(vLineX2,vLineY2,'k')

plot(figX1,figY1,'bo')

axis('square',[-8,8,-4,12]);
hold off;     
