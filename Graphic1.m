%% Übung Graphic I 
% Autor James Mayr
clear;clc;

%Aufgabe 1 Funktionsgraph
%% 1a) Plotte die Sinuskurve (Farbe blau) im Intervall [0; 6π] (help plot). Taste dafur die 
% Sinusfunktion an 1081 an aquidistanten Stutzstellen ab (linspace).
x = linspace(0,6*pi,1081);
y = sin(x);
figure(1);
plot(x,y);
%% 1b) Wähle nun den Plotausschnitt so, dass er durch den Funktionsgraphen ausgefullt
% wird (axis tight).
axis tight;

%% 1c) Zeichne in derselben Graphik (hold) auch noch die 
% Nullstellen und Extremalpunkte der Sinusfunktion ein (Farbe schwarz)

hold on;

x1 = 0:pi/2:6*pi;
y1 = sin(x1);
plot(x1,y1,'ko');


%% 1d) Verbinde die Nullstellen und Extremalstellen in der Graphik durch lineare Interpolation (Farbe rot)
plot(x1,y1,'r');

%% 2e) Gib der Graphik den Titel "Linear interpolierte Sinusfunktion"(title).
title('Linear interpolierte Sinusfunktion');

%% 2f) Schliesse die Plotfigur (close).
hold off;


%Aufgabe 2 Mehrere Funktionsgraphen
%% 2a.) Die Cosinusfunktion cos x wird in der N¨ ahe von x = 0? optimal durch ihre Taylorpolynome
% Sum(1,n,( ((-1)^k)/(2k!))*x^(2*k)
figure(1);
a2x = linspace(0,pi,1000);
a2y = cos(a2x);

%calculate Taylor Serie from 0 to 6
pMatrix = zeros(6,1000);
for i=0:6
    if(i==0)
        pMatrix(i+1,:) =( ((-1).^(i))./(factorial(i*2)) ).*a2x.^(i*2);
    else
        pMatrix(i+1,:) = pMatrix(i,:)+ ( ((-1).^(i))./(factorial(i*2)) ).*a2x.^(i*2);
    end
end
plot(a2x,a2y,'b',a2x,pMatrix(2,:),'r',a2x,pMatrix(3,:),'r');
hold on;
%% 2b) Fuge nun dem Plot noch den Graphen des Polynoms  p6 hinzu (grun)
plot(a2x,pMatrix(4,:),'g');

%% 2c) Schränke nun den Plot graphisch auf das Fenster [0; π 2 ] horizontal und [0; 1] vertikal ein (axis).
axis([0,pi/2,0,1]);
hold off;


%Aufgabe 3 Mehrere Plotfenster
%% Erzeuge vorab die 6 Taylorpolynome p2 - p6 der Cosinusfunktion auf dem Intervall 
% [0; 2π] als Zeilen einer Matrix P:
figure(2);

%Erzeuge den Vektor x = linspace(0,2*pi,361)
x = linspace(0,2*pi,361);


%Erzeuge die (6,361)-Matrix mit Zeilen x und daraus durch Arraypotenzierung die
%Matrix mit den Zeilen x2k (k = 1; : : : ; 6).
% Serie beginnt aber bei 0 und nicht 1
x2k = zeros(7,361);
for j=1:7
   x2k(j,:) =x.^(2*(j-1));
end

%Erzeuge nun, durch Multiplikation mit einer Diagonalmatrix, die Matrix mit den
%Zeilen
i = 0:6;
diagMat = diag(( ((-1).^(i))./(factorial(i*2)) ));

P = diagMat * x2k;
P = cumsum(P); %sum up taylor serie
 

%% 3a Plotte in 6 vertikal gestapelten Plotfenstern (subplot) die Cosinusfunktion auf
% dem Intervall [0; 2π], je zusammen mit einem approximierenden Taylorpolynom
% p2k (k = 1; : : : ; 6). Schr¨ anke dabei den vertikalen Bildausschnitt auf das Intervall
hold on;
for i=1:6
    subplot(1,6,i);
    plot(x,cos(x),'b',x,P(i,:),'r');
    axis([0,2*pi,-1,1]);
    
    %%  3b) Gib nun dem k-ten Plotfenster den Titel "Cosinusfunktion mit
    % Taylorpolynom 2pk
    title(['Cosfnk mit Taylor p ',num2str(2*i)]);
end
hold off;
%% 3c Plotte in einer neuen Figur (figure) mit 6 Plotfenstern je die Fehlerfunktion
% p2k(x) − cos x (x 2 [0; 2π])

figure(3);
for i=1:6

    subplot(1,6,i);
    approx = (P(i+1,:)-cos(x));
    plot(x,approx,'b');
    axis([0,2*pi,-1,1]);
    
    % b) Gib nun dem k-ten Plotfenster den Titel "Cosinusfunktion mit
    % Taylorpolynom 2pk
    title(['Approx Taylor p ',num2str(2*i)]);
end


%Aufgabe 4
%Multiple Plots mit Matrizen Erzeuge vorab eine (721,36)-Matrix, welche
%in der k-ten Spalte die Sinusfunktion enth¨ alt, je verschoben um (k −1)·10◦ nach rechts
%(k = 1; : : : ; 36), alle auf dem Intervall [0; 4π]

figure(4);
SinP = ones(721,36);

x =linspace(0,4*pi,721);
i = 1:36; 
for i=1:36
    SinP(:,i) = sin((x+((i-1)*deg2rad(10))));
end
%Graphics

%% 4a) Plotte die 36 Funktionsgraphen zusammen im selben Plotfenster
for y=1:36
    if(y ==2)
        hold on;
    end
    plot(x,SinP(:,y),'b');
end
axis([0,4*pi,-1,1]);
hold off;
figure(5);
%% 4b) Plotte die Funktionsgraphen noch einmal, alle schwarz.
for y=1:36
    if(y ==2)
        hold on;
    end
    plot(x,SinP(:,y),'k');
end
axis([0,4*pi,-1,1]);
hold off;




