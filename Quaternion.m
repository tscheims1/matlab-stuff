classdef Quaternion
   properties
       real = 0;
       i = 0;
       j = 0;
       k = 0;
   end
   methods
       function r = Quaternion(vector4)
            sizeVector4 = size(vector4);
         
         
         for y=1:sizeVector4(1,1)
             if(sizeVector4(1,2) == 3)
                 r.real= 0;
                 r.i = vector4(y,1);
                 r.j = vector4(y,2);
                 r.k = vector4(y,3);
             else
                 r.real= vector4(y,1);
                 r.i = vector4(y,2);
                 r.j = vector4(y,3);
                 r.k = vector4(y,4);
             end
             rxy(y,1) = r;
         end
         r = rxy;
        
       end
       function r = plus(q1,q2)
        q1.real = q1.real + q2.real;
        q1.i = q1.i + q2.i;
        q1.j = q1.j + q2.j;
        q1.k = q1.k + q2.k;
        r = Quaternion([q1.real,q1.i,q1.j,q1.k]);
        
       
       end
       function r = minus(q1,q2)
         r.real = q1.real - q2.real;
         r.i = q1.i - q2.i;
         r.j = q1.j - q2.j;
         r.k = q1.k - q2.k;
       
       end
       %% Matrice Multiplication
       function r = mtimes(q1Mat,q2Mat)
         size1 = size(q1Mat);
         size2 = size(q2Mat);
         
        if ~isequal(size1,size2)
            throw(MException('Quaternion:Multiply','Matrices must have same size'));
        end
        
        r = q1Mat; %Create Matrice of same Size
        for x=1:size1(1,1)
            for y=1:size1(1,2)
               tmp = q1Mat(x,y).multiply(q2Mat(x,y));
               r(x,y) = Quaternion([tmp.real,tmp.i,tmp.j,tmp.k]);
            end
        end
        
       end
       %% cell multiplication
       function r = times(q1Mat,q2Mat)
        size1 = size(q1Mat);
         size2 = size(q2Mat);
         
        if ~isequal(size1,size2)
            throw(MException('Quaternion:Multiply','Matrices must have same size'));
        end
        
        
        for x=1:size1(1,1)
            for y=1:size1(1,2)
                q1 = q1Mat(x,y);
                q2 = q2Mat(x,y);
                if(isa(q2,'Quaternion') && isa(q1,'Quaternion'))
                    r = q1Mat; %Create Matrice of same Size
                    q1.real = q1.real*q2.real;
                    q1.i = q1.i*q2.i;
                    q1.j = q1.j*q2.j;
                    q1.k = q1.k*q2.k;
                    r(x,y) = Quaternion([q1.real,q1.i,q1.j,q1.k]);
                elseif(isa(q1,'Quaternion'))
                    r = q1Mat; %Create Matrice of same Size
                    q1.real = q1.real*q2;
                    q1.i = q1.i*q2;
                    q1.j = q1.j*q2;
                    q1.k = q1.k*q2;   
                    r(x,y) = Quaternion([q1.real,q1.i,q1.j,q1.k]);
                else
                   r = q2Mat; %Create Matrice of same Size
                   q2.real = q2.real*q1;
                   q2.i = q2.i*q1;
                   q2.j = q2.j*q1;
                   q2.k = q2.k*q1;    
                   r(x,y) = Quaternion([q2.real,q2.i,q2.j,q2.k]);
                end
               
            end
        end
       end
       %%
       function r = Negation(q1)
        q1.real = -q1.real;
        q1.i = -q1.i;
        q1.j = -q1.j;
        q1.k = -q1.k;
        
        r = q1;
       
       end
       
       %% 
       function r = Conjugation(q1)
           q1Size = size(q1); 
           r = q1;
           for x=1:q1Size(1,1)
                for y=1:q1Size(1,2)
                    r(x,y) = Quaternion([q1(x,y).real, -q1(x,y).i,-q1(x,y).j,-q1(x,y).k]);
                end
            end
       end
       %% 
       function r = Magnitude(q1)
            r = sqrt(q1.real^2+q1.i^2+q1.j^2+q1.k^2);
       end
       function r = Normalize(q1)
           
            q1Size = size(q1);
            r = q1;
            for x=1:q1Size(1,1)
                for y=1:q1Size(1,2)
                    mag = q1(x,y).Magnitude();
                    real = q1(x,y).real ./mag;
                    i =  q1(x,y).i/ mag;
                    j = q1(x,y).j/ mag;
                    k = q1(x,y).k/ mag;
                    r(x,y) = Quaternion([real,i,j,k]);
                end
            end
       end
       function r = ToVector3(q1)
        q1Size = size(q1);
        r = zeros(q1Size(1,1),3*q1Size(1,2));
        for x=1:q1Size(1,1)
            for y=1:q1Size(1,2)
                r(x,(y-1)*3+1)   = q1(x,y).i;
                r(x,(y-1)*3+2) = q1(x,y).j;
                r(x,(y-1)*3+3) = q1(x,y).k;
            end
        end
       end
   end
   methods(Access=protected)
          function r = multiply(q1,q2)
            % i^2 = j^2 = k ^2 = -1
            r.real =   (-1)*(q1.i*q2.i) + (-1)*(q1.j*q2.j) + ...
                        (-1)*(q1.k*q2.k) + q1.real*q2.real;
                    
            % j*k = i AND k*j = -i
            % real*i = i AND i*real = i
            r.i = (q1.j*q2.k)-(q1.k*q2.j) + ...
                 q1.real*q2.i + q1.i * q2.real;
             
            % k*i = j AND i*k = -j
            % real*j = j AND j*real = j
            r.j = (q1.k*q2.i) - (q1.i*q2.k) +...
                q1.real*q2.j +q1.j*q2.real;
            
            % i*j = k AND j*i = -k
            % real*k = k AND k*real = k
            r.k = (q1.i*q2.j) -(q1.j*q2.i) +...
                q1.real*q2.k + q1.k*q2.real;
       end
   end
end