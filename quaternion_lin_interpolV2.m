close all; clc; clear;
%% Linear interpolation

v = [2,1,1,2;
    0,0,1,0;]
        
vFin = [-2,-1,-1,-2;
         0,-1,0,0;]

vFinT = [-2,-2,-2,-2;
        3.5,3.5,3.5,3.5;]
vFin = vFin +vFinT;


k = 30;
figure(1);



phi = pi;
angleq1 = [cos(phi/2),0*sin(phi/2),0*sin(phi/2),1*sin(phi/2)];
phi = 0;
angleq0 = [cos(phi/2),0*sin(phi/2),0*sin(phi/2),1*sin(phi/2)];



q0 = Quaternion(angleq0);
q1 = Quaternion(angleq1);

d1 = Quaternion([0,-2,3.5,0]*.5);
d1 = d1*q1;

d0 = Quaternion([0,0,0,0]);

p1 = DualQuaternion(q1,d1);
p0 = DualQuaternion(q0,d0);

for i=0:k
    t = i/k;
    
    % Linear interpolation
    p3 = ((1-t).*p0+t.*p1);
    
    % the dual quaternion must be 1 (or the interpolation will scale too)
    p3 = p3.Normalize();
    
    v2 = [];
    for j=1:size(v,2)
        v1d = Quaternion([0,v(1,j),v(2,j),0]);
        v1r = Quaternion([1,0,0,0]);
        v1 = DualQuaternion(v1r,v1d);
        %dual Quaternion Sandwich Operation
        p4 = p3*v1*p3.Conjugation().Negation();
        %% 
        v2 = [v2,[p4.d.i,p4.d.j]'];
        
    end
    hold on;
    figure(1);
    plot(v2(1,:),v2(2,:));
    figure(2);
    plot(v2(1,1),v2(2,1),'b+');
    plot(v2(1,2),v2(2,2),'r+');
    plot(v2(1,3),v2(2,3),'g+');
end

figure(1);
plot([-10,10],[0,0],'k');
plot([0,0],[-10,10],'k');
plot(vFin(1,:),vFin(2,:),'LineWidth',2);
plot(v(1,:),v(2,:),'LineWidth',2);
title('dual quaternion interpolation');
axis([-5,4,-1,6]);


figure(2);
plot([-10,10],[0,0],'k');
plot([0,0],[-10,10],'k');
plot(vFin(1,:),vFin(2,:),'LineWidth',2);
plot(v(1,:),v(2,:),'LineWidth',2);
title('dual quaternion interpolation');


axis([-5,4,-1,6]);
hold off;
