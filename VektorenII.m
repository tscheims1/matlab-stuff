%% Übung Vektoren II 
% Autor James Mayr
clear;clc;


% Aufgabe 1 Array-Operationen Erzeuge die 19-dimensionalen Vektoren vs, vq und vd:
%% 1a) vs = [1; 4; : : : ; 324; 361]
vs = ((1:19).^2)'

%% 1b) vq = [ 1 2; 1 4; : : : 36 1 ; 38 1 ]

vq = 2:2:38;
vq = (1./vq)'

%% 1c) vd = [p2 − 1; p5 − 2; : : : p325 − 18; p362 − 19]

vd = (sqrt((1:19).^2+1) - (1:19))'

%Vergleiche die Vektoren vq und vd:
%% 1d) Bilde die Vektordifferenz vq und vd
d1 = vq - vd

%% 1e  Bilde den Arrayquotienten vd/vq
e1 = vd./vq

% Aufgabe 2 Elementare Funktionen von Vektoren
%% 2a) Taste die Sinusfunktion an den Stellen (n*pi)/4(n = 0; : : : ; 100) ab

a2 = sin((0:100).*(pi/4))'

%% 2b) Bilde den Vektor s = [sin(n)](n = 1; : : : ; 100) und stelle ihn graphisch dar(plot)

x =( 1:100);
s = (sin(x))
figure(1)
plot(x,s);

%% 2c Bilde den Vektor s1, in welchem die Eintr¨ age von s nach Gr¨ osse geordnet sind (sort)
s1 = sort(s)

%% 2d Bilde den Vektor t bestehend aus 100 ¨ aquidistanten Stutzstellen im Intervall − π /2 ; π /2 (linspace)
t = linspace(-pi/2,pi/2,100)

%% 2e) Bilde den Vektor s2 = sin(t)

s2 = sin(t)

%Vergleiche s1 und s2 (Gleichverteilungssatz von H. Weyl ):
%% 2f) Plotte die beiden Vektoren zusammen
%% g) Bilde und plotte die Differenz der beiden Vektoren
figure(2);
plot(x,s1,'r');
hold on;
plot(x,s2,'b');
plot(x,(s1-s2),'k');
hold off;


% Aufgabe 3 Repetition
%% Erzeuge zuerst den Vektor v1 [1; : : : ; 20] und dann rein mathematisch (ohne Fallunterscheidung) nacheinander die folgenden Vektoren:

v1 = (1:20)';

%% 3a) a = [1; 1/2 ; : : : 1/20 ]
a3 = 1./v1

%% 3b) b = [1; −1; : : : ; 1; −1]
b3 = (-ones(20,1)).^(v1+1)

%% 3c) c = [1,1/2,3,1/4..]
c3 = (v1).^(b3)


%Aufgabe 4 Skalarwertige Funktionen von Vektoren
% Gegeben sind die 100 Zahlen cos(n)(n = 1; :::; 100)
%% 4a) Bestimme das Minimum min und das Maximum max dieser Zahlen
v2 = cos(1:100);

a4max = max(v2)
a4min = min(v2)

%% 4b) Bestimme den Mittelwert mean und die Standardabweichung std dieser Zahlen
b5mean = mean(v2)
b5std = std(v2)

% Aufgabe 5 Summenbefehle (sum, cumsum)
%% 5a) Vergleiche die Summe P1000 k=1 k 1 mit der Zahl ln(1000)

sum1000 = sum(1./(1:1000))
ln1000 = log(1000)
a5Diff = abs(sum1000-ln1000)
%--> Reihe divergiert, also keine Annäherung an ln(1000)

%% 5b Vergleiche die Summe P1000 k=1 ((−1)^(k-1)/k) mit der Zahl ln(2)
b5k = 1:1000;
counter = (-ones(1,1000)).^(b5k-1);
b5Serie = counter./b5k;
sum1000_2 = sum(b5Serie)
ln2 = log(2)
b5Diff = abs(sum1000_2-ln2)
%diese Reihe konvergiert gegen ln2 (siehe  Leibniz-Kriterium)

%% 5c Vergleiche die Summe(1,1000,1/k^2) mit der Zahl (pi^2)/6
c5k = 1:1000;
c5Serie =1./(b5k.^2);
sum1000_3 = sum(c5Serie)
number = (pi^2)/6
c5Diff = abs(sum1000_3-number)
% konvergiert gegen (pi^2)/6
%% 5d Bilde und plotte den Vektor aus 5b

x = 1:1000;
d5y = cumsum(b5Serie);
figure(3);
plot(x,d5y,'b');
% ---> alternierende Reihe konvergiert
%% 5e Bilde und plotte den Vektor aus 5c
figure(4);
e5y = cumsum(c5Serie);
plot(x,e5y,'b');
% Reihe konvergiert
