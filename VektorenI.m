%% Übung Vektoren I 
% Autor James Mayr
clear;clc;

%Aufgabe 1 Eingabe von Vektoren, Zugriff auf Vektorkomponenten
%% 1a.)Gib die Zeilenvektoren a = [1; 7; 4; 2] und b = [3; 0; 1; 0] ein

a = [1; 7; 4; 2];
b = [3; 0; 1; 0];

%% 1b.) Bilde den 13-dimensionalen Zeilenvektor
u = [a - b; a; 0; a + b];

%% 1c.) Gib nacheinander die Komponenten u0; u1; u11 und u14 von u aus
% (Ich gehe davon aus, dass u0 das erste Element des Vektors u ist)
disp(['u0:',num2str(u(1,1))]); 
disp(['u2:',num2str(u(2,1))]);
disp(['u12',num2str(u(12,1))]);
%u(15,1) %dimensionsfehler: letztes Element ist u(13,1)

%% 1d.)  Bilde aus u die Vektoren
u1 = u(1:8);
u2 = u(6:13);
u3 = [u(2,1),u(5,1),u(2,1),u(7,1)];

%% 1e.)( Bestimme die Dimension von u (length, size))
disp(['length of u: ',num2str(length(u))]);
disp(['size of u: ',num2str(size(u))]);

%% 1f.) Versuche, die Vektorsumme a + u zu bilden

%a+u; // klappt nicht: dimensionfehler d. matrixen

%% 1g.) Andere den Vektor u schrittweise ab:

%Setze u4 = 3 
u(4,1) =3;
%Eliminiere die Komponente u5
u = [u(1:4);u(6:13)];

%Addiere 1 zur Komponente u6
u(6,1) = u(6,1)+1;

%Setze neu u20 = 1
u(20,1) = 1;


%Aufgabe 2 Erzeugung von Vektoren (zeros, ones)
%% 2a.) Erzeuge den 23-dimensionalen Zeilenvektor
a2 = [ones(6,1);zeros(4,1);-1*ones(5,1);1;2*ones(7,1)]

%% 2b.) Erzeuge den Vektor [1; 0; : : : ; 0; 1] der Dimension 100
b2 = [ones(1,50);zeros(1,50)];
b2 = b2(:)

%% 2c.)  Erzeuge den 32-dimensionalen Vektor [1; 2; : : : ; 31; 32]
c2 = (1:32)'

%% d) Erzeuge den 9-dimensionalen Spaltenvektor zu[3; 5; :::; 17; 19]
d2 = 3:2:19


%% 2e) Erzeuge (ohne Ausgabe) den 667-dimensionalen Zeilenvektor
%x = [999; 996; : : : ; −996; −999] Kontrolliere die Dimension und die drittletzte
%Komponente von x.
x = (999:-3:-999)';

x(665,1)

%% 2f) Erzeuge den 100-dimensionalen Vektor [0; 1; : : : ; 0; 1]
f2 = [zeros(1,50);ones(1,50)];
f2 = f2(:)

%% 2g Erzeuge den 100-dimensionalen Vektor [1; −1; : : : ; 1; −1]
g2 = [ones(1,50);-1*ones(1,50)];
g2 = g2(:)