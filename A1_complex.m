%% Rotation in C

%Figure as a Complex Number
figXY = [1+i,2+i,3+i,3+2*i,2+2*i,1+2*i,1+3*i,2+3*i,3+3*i];
m = linspace(0+2i,0+2i,9);
 
%rotation angle in radiant
phi = pi/3;

%prepare for rotation around origin
figXY1 = figXY-m;

%convert complex numbers in polar form:
% r*(cos phi+sin phi)
% (ploar form is more readable than the exp form)
l = sqrt(real(figXY1).^2+imag(figXY1).^2);
angle = atan(imag(figXY1)./real(figXY1));


figXY2 = l.*(cos(angle+phi)+i*sin(angle+phi))+m;

%% Graphics
plot(real(figXY),imag(figXY),'ro')
hold on;
plot(real(figXY2),imag(figXY2),'bo')

plot(linspace(10,-10,10),linspace(0,0,10),'k')
plot(linspace(0,0,10),linspace(10,-10,10),'k')

axis('square',[-8,8,-4,12]);
hold off;