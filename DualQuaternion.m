classdef DualQuaternion
   properties
       r
       d
   end
   methods
       function r = DualQuaternion(a,b)
           
           if(isa(a,'Quaternion'))
            r.r = a;
            r.d = b;
           else
            r.r = Quaternion(a);
            r.d = Quaternion(b);    
           end
            
       end
       function r = plus(q1,q2)
        q1.r = q1.r + q2.r;
        q1.d = q1.r + q2.d;
        r = DualQuaternion(q1.r,q1.d);
       end
       function r = minus(q1,q2)
         q1.r = q1.r - q2.r;
         q1.d = q1.d - q2.d;
         
         r = DualQuaternion(q1.r,q1.d);
         
       end
       %% Matrice Multiplication
       function r = mtimes(q1Mat,q2Mat)

         qr = q1Mat.r * q2Mat.r;
         qd = (q1Mat.r * q2Mat.d + q1Mat.d * q2Mat.r); 
         
         r = DualQuaternion(qr,qd);     

       end
       %% cell multiplication
       function r = times(q1Mat,q2Mat)

        if(isa(q1Mat,'DualQuaternion') && isa(q2Mat,'DualQuaternion'))   
            qr = q1Mat.r .* q2Mat.r;
            qd = q1Mat.d .* q2Mat.d;

        elseif isa(q1Mat,'DualQuaternion')
            qr = q1Mat.r.times(q2Mat);
            qd = q1Mat.d.times(q2Mat);
        else
            qr = q2Mat.r.times(q1Mat);
            qd = q2Mat.d.times(q1Mat);
        end
        
        r = DualQuaternion(qr,qd);
       end
       function r = Negation(q1)
            
        r = DualQuaternion(q1.r,q1.d.Negation());
       end
       
       %%
       function r = Conjugation(q1)
           qr = q1.r.Conjugation();
           qd = q1.d.Conjugation();
           
           r = DualQuaternion(qr,qd);
       end
       function r = Normalize(q1)
       
           normQ = 1/q1.r.Magnitude();
           qr = q1.r.*normQ;
           qd = q1.d.*normQ;
           r = DualQuaternion(qr,qd);
       
       end
   end
end