clc; clear;
% Rotate in H

p = PlotHelper();
[vert,edges] = p.Pyramid3([0.7,0.7,0.7]);

rotVector = [1,1,0];
rotVector = rotVector/norm(rotVector);

phi = pi/2;

sVert = size(vert,1);
vertRot = zeros(sVert,3);
for i=1:sVert
    r1 = [cos(phi/2),sin(phi/2),sin(phi/2),sin(phi/2)].*[1,rotVector];
    r = Quaternion(r1);
    v1 = Quaternion([0,vert(i,:)]);
    
    q1 = r*v1*r.Conjugation();
    vertRot(i,:) = q1.ToVector3();
end

%% Graphics
trimesh(edges,vert(:,1),vert(:,2),vert(:,3));


%static blue colormap
map = [0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3];
colormap(map);

hold on;

trimesh(edges,vertRot(:,1),vertRot(:,2),vertRot(:,3));
p.PlotLine3(rotVector,10);
axis ([-3,3,-3,3,-3,3]);
axis equal;

ax = gca;

ax.ZGrid = 'on';
ax.XGrid = 'on';
ax.YGrid = 'on';
ax.ZMinorGrid = 'on';
ax.XMinorGrid = 'on';
ax.YMinorGrid = 'on';

hold off;
