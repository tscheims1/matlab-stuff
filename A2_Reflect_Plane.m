%% Homogenous Reflection and Projection onto plane3
p = PlotHelper();
[vert,edges] = p.Pyramid3([0.7,0.7,1.2]);



%Calculate Plane
P1 = [0.5,0.5,0.5];
P2 = [2,0,1.5];
P3 = [2,2,1];
[A,B,C,D] = p.Plane3(P1,P2,P3);

h = HomogenousFigure3(vert');
h = h.ReflectPlane3(A,B,C,D);

h2 = HomogenousFigure3(vert');

h2 = h2.ProjectOntoPlane(P2,P3,P1);

%% Graphics
trimesh(edges,vert(:,1),vert(:,2),vert(:,3));

%static blue colormap
map = [0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3];
colormap(map);

hold on;
p.PlotPlane3(A,B,C,D,P1,P2,P3);
fig = h.fig';
trimesh(edges,fig(:,1),fig(:,2),fig(:,3));

fig = h2.fig';
trimesh(edges,fig(:,1),fig(:,2),fig(:,3));

axis ([-3,3,-3,3,-3,3]);
axis equal;

ax = gca;

ax.ZGrid = 'on';
ax.XGrid = 'on';
ax.YGrid = 'on';
ax.ZMinorGrid = 'on';
ax.XMinorGrid = 'on';
ax.YMinorGrid = 'on';
xlabel('x');
ylabel('y');
zlabel('z');
hold off;
