%% Homogenous Shear Scale and Perspective 3D
p = PlotHelper();
t = [0.7,0.7,0.7];
[vert,edges] = p.Pyramid3(t);

h = HomogenousFigure3(vert');
h = h.Scale([1,2,3],t);


t = [3,4,5];
[vert2,edges] = p.Pyramid3(t);

h2 = HomogenousFigure3(vert2');
h2 = h2.ShearXY(1.5,2,t);

[cubeVert,CubeEdges] = p.Cube3([0,0,0]);


%% Graphics
trimesh(edges,vert(:,1),vert(:,2),vert(:,3));
trimesh(edges,vert2(:,1),vert2(:,2),vert2(:,3));


%static blue colormap
map = [0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3
    0, 0, 0.3];
colormap(map);

hold on;
fig = h.fig';
fig2 = h2.fig';
trimesh(edges,fig(:,1),fig(:,2),fig(:,3));
trimesh(edges,fig2(:,1),fig2(:,2),fig2(:,3));


axis ([-3,5,-3,5,-3,5]);
axis equal;

ax = gca;

ax.ZGrid = 'on';
ax.XGrid = 'on';
ax.YGrid = 'on';
ax.ZMinorGrid = 'on';
ax.XMinorGrid = 'on';
ax.YMinorGrid = 'on';

hold off;
