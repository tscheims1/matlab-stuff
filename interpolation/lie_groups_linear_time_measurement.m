close all; clc; clear;
%% rotation
v = [0,1,1,0,0;
     0,0,1,1,0];
v = v+[1,1,1,1,1;0,0,0,0,0];

 
 
%% Linear Interpol rot

phi = pi/2;


R = [cos(phi),-sin(phi);
      sin(phi),cos(phi)];

I = eye(2); 

k = 10;
tic;
for x=0:10000
for i=0:k
    t = i/k;
    interpol =   (1-t)*I+t*R;
    v2 = interpol *v;
   
end
end


toc
 
 

%% Exp Interpolation rotation
phi = pi/2;


S = eye(2);

tic;
for x=0:10000
for i=0:k
    t = i/k;
    phiT = phi*t;
    R = [cos(phiT),-sin(phiT);
      sin(phiT),cos(phiT)];
    
    interpol = R*exp(t*log(S));
    v2 = interpol *v;
end
end
toc

