close all; clc; clear;
%% Linear interpolation


%R= [2,3;
%    0,3];
R = [20,0;
     0,20;];

I = eye(2); 
v = [0,1,1,0,0;
     0,0,1,1,0];
 
vt = [1,1,1,1,1;
     0,0,0,0,0;];
v = v + vt;
k = 10;
figure(1);
title('quaternion linear interpolation');

phi = pi/2;
angleq1 = [cos(phi/2),0*sin(phi/2),0*sin(phi/2),1*sin(phi/2)];
phi = 0;
angleq0 = [cos(phi/2),0*sin(phi/2),0*sin(phi/2),1*sin(phi/2)];

q0 = Quaternion(angleq0);
q1 = Quaternion(angleq1);
for i=0:k
    t = i/k;
    
    q3 = ((1-t).*q0+t.*q1);
    q3 =(1/q3.Magnitude()).*q3;
    
    v2 = [];
    for j=1:size(v,2)
        v1 = Quaternion([0,v(1,j),v(2,j),0]);
        q4 = q3*v1*q3.Conjugation();
        v2 = [v2,[q4.i,q4.j]'];
        
    end
    hold on;
    plot(v2(1,:),v2(2,:),'LineWidth',2);
end


t=0:0.01:1;
s=sin(2*pi*2*t);
c=cos(2*pi*2*t);
hold on;
plot(s,c,'k');

axis equal;
axis tight;
hold off;
