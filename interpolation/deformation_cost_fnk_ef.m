clc;clear;close all;

        
Triangle2 = [0,1,1,0;
            0,0,1,0;
            1,1,1,1];
Triangle3 =  [1,1,2,1;
              0,1,0,0;
              1,1,1,1];
       

        
Triangle2Fin = [0,1,1,0;
            0,0,1,0;
            1,1,1,1];
Triangle3Fin =  [1,1,2,1;
                0,1,1,0;
                 1,1,1,1];

 %calculate the transformation matrix
 A1 =  Triangle2Fin(1:3,1:3) *inv(Triangle2(1:3,1:3));  
 A2 =  Triangle3Fin(1:3,1:3) *inv(Triangle3(1:3,1:3));  
 
 
 
 
 %decompose the matrix into a rotation and symetric matrix with polar
 %decomposing
 [R1,S1] = poldecomp(A1(1:2,1:2));
 [R2,S2] = poldecomp(A2(1:2,1:2));

 phi1 = acos(R1(1,1));
 phi2 = acos(R2(1,1));
 
 k = 5;
 figure(1)

 for i=0:k-1
    t = i/k;
    phiT1 = phi1*t;
    phiT2 = phi2*t;
    
    R1 = [cos(phiT1),-sin(phiT1);
      sin(phiT1),cos(phiT1)];
    
    R2 = [cos(phiT2),-sin(phiT2);
      sin(phiT2),cos(phiT2)];
    
  
    A1 = R1^t*S1^t; % local linear interpolation
    A2 = R2^t*S2^t; % local linear interpolation
    
    % the minimize is easy!
    B1 = (A1+A2)./2;
    
    
    
    b12_11 = B1(1,1);
    b12_12 = B1(1,2);
    b12_21 = B1(2,1);
    b12_22 = B1(2,2);
    
    b1_11 = A1(1,1);
    b1_12 = A1(1,2);
    b1_21 = A1(2,1);
    b1_22 = A1(2,2);
    
    b2_11 = A2(1,1);
    b2_12 = A2(1,2);
    b2_21 = A2(2,1);
    b2_22 = A2(2,2);
    
    
    p1x = Triangle2(1,1);
    p1y = Triangle2(2,1);
    q1x = Triangle2Fin(1,1);
    q1y = Triangle2Fin(2,1);
       
    % solving the global translation
    c1 = (1-t-b1_11)*p1x + q1x*t + b1_12*p1y;
    f1 = (1-t-b1_22)*p1y + q1y*t + b1_21*p1x;
    

    
    B12 = [B1,[c1,f1]'];
    A1 = [A1,[c1,f1]'];
    A2 = [A2,[c1,f1]'];

    %appying the interpolation
    v1 = (A1 *Triangle2(:,1));
    v12 = (B12 *Triangle2(:,2:3));
    v2 = A2*Triangle3(:,3);
    v21 = (B12 *Triangle3(:,1:2));
    
    v1 = real([v1,v12,v1]);
    v2 = real([v2,v12,v2]);
    
    hold on;
    subplot(2,2,1);
    plot(v1(1,:),v1(2,:));
    plot(v2(1,:),v2(2,:));
    
    subplot(2,2,2);
    hold on;
    v1 = A1*Triangle2;
    v2 = A2*Triangle3;
    plot(v1(1,:),v1(2,:));
    plot(v2(1,:),v2(2,:));
    subplot(2,2,1);
    %plot(v12(1,:),v12(2,:));

    end

 %% graphics        
 subplot(2,2,1);
 title('Cost-Fnk E^F');
 hold on;
 plot([-10,10],[0,0],'k');
 plot([0,0],[-10,10],'k');
 axis([0,4,-1,3]);
 
 subplot(2,2,2);
 title('local Interpolation');
 plot([-10,10],[0,0],'k');
 plot([0,0],[-10,10],'k');
 axis([0,4,-1,3]);
 
 subplot(2,2,3);
 title('Object t_0');

 hold on;
 plot(Triangle2(1,:),Triangle2(2,:));
 plot(Triangle3(1,:),Triangle3(2,:));
 plot([-10,10],[0,0],'k');
 plot([0,0],[-10,10],'k');
 axis([0,4,-1,3]);

 subplot(2,2,4);
 title('Object t_1');
 hold on;
 plot(Triangle2Fin(1,:),Triangle2Fin(2,:));
 plot(Triangle3Fin(1,:),Triangle3Fin(2,:));
  plot([-10,10],[0,0],'k');
 plot([0,0],[-10,10],'k');
  axis([0,4,-1,3]);


 
  hold off;
