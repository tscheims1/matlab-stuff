close all; clc; clear;

Triangle2 = [2,1,1,2;
            0,0,1,0;
            1,1,1,1];
        
Triangle2Fin = [-1,0,0,-1;
                 1,1,0,1;
                 1,1,1,1];
Triangle2FinT = [-3,-3,-3,-3;
                 2.5,2.5,2.5,2.5;
                 1,1,1,1,];  
Triangle2Fin = Triangle2Fin + Triangle2FinT;

 %calculation the transformation matrix            
 A =  Triangle2Fin(1:3,1:3) *inv(Triangle2(1:3,1:3));  
 
 %decompose the matrix into a rotation and symetric matrix with polar
 %decomposing
 [R,S] = poldecomp(A(1:2,1:2));
 phi = acos(R(1,1));
 k = 30;
 for i=0:k
    t = i/k;
    phiT = phi*t;
    R = [cos(phiT),-sin(phiT);
      sin(phiT),cos(phiT)];
    
    A = R*exp(t*log(S)); % local linear interpolation
    a11 = A(1,1);
    a12 = A(1,2);
    a21 = A(2,1);
    a22 = A(2,2);
    p1x = Triangle2(1,1);
    p1y = Triangle2(2,1);
    q1x = Triangle2Fin(1,1);
    q1y = Triangle2Fin(2,1);
    
    %solving global translation
    c = (1-t-a11)*p1x + q1x*t + a12*p1y;
    f = (1-t-a22)*p1y + q1y*t + a21*p1x;
    
    B = [A,[c,f]'];
    
    %appying the interpolation
    v2 = (B *Triangle2(1:3,:)); 
    hold on;
    figure(1);
    plot(v2(1,1),v2(2,1),'b+');
    plot(v2(1,2),v2(2,2),'r+');
    plot(v2(1,3),v2(2,3),'g+');
    figure(2);
    plot(v2(1,:),v2(2,:));

 end
%% graphics 
        

 hold on;
 figure(1);
 plot([-10,10],[0,0],'k');
 plot([0,0],[-10,10],'k');
 figure(2);
 
 plot([-10,10],[0,0],'k');
 plot([0,0],[-10,10],'k');
 
 plot(Triangle2(1,:),Triangle2(2,:));
 
 phi = pi/2;
 R1 = [cos(phi),-sin(phi);
    sin(phi),cos(phi)];
 S = [1,1.1;
      0,1];

  
 plot(Triangle2(1,:),Triangle2(2,:),'Linewidth',2);
 plot(Triangle2Fin(1,:),Triangle2Fin(2,:),'Linewidth',2);
 axis([-5,4,-1,6]);
 figure(1);
 plot(Triangle2(1,:),Triangle2(2,:),'Linewidth',2);
 plot(Triangle2Fin(1,:),Triangle2Fin(2,:),'Linewidth',2);
 axis([-5,4,-1,6]);
 hold off;
 
 