classdef PlotHelper
   methods 
       function p = PlotHelper()
           
       end
       %% Plot XY Axis
       function PlotAxis(obj)
            
            %x and y axis
            plot(linspace(10,-10,10),linspace(0,0,10),'k');
            plot(linspace(0,0,10),linspace(10,-10,10),'k');

       end
       %% Plot Line Equation m*x+q
       function PlotLine(obj,m,q)
        
            %mx+q line
            lineX = -10:0.5:10;
            lineY = m.*lineX+q;
            plot(lineX,lineY,'k')    
       end
       %% Plot Line2 with Points
       function PlotLine2(obj,x1,y1,x2,y2)
           
          
           plot([x1,x2],[y1,y2],'k');
           
       end
       %% Plot Line3 with a direction Vector
       function PlotLine3(obj,direction,scaling)
       
        direction = direction/norm(direction);
        
        
        p1 = direction'.*-(scaling/2);
        p2 = direction'.*(scaling/2);

        plot3([p1(1,:),p2(1,:)],[p1(2,:),p2(2,:)],[p1(3,:),p2(3,:)],'k');
        
       end
       function PlotFigure(obj,figure1,color)
           plot(figure1(1,:),figure1(2,:),color);
       end
       %% Create Vertices and Eges for a Pyramid
       function [vert,edges] = Pyramid3(obj,transVector)

        vert = [0,0,0;
                1,0,0;
                1,1,0;
                0,1,0;
                0.5,0.5,1];
         vert = vert +repmat(transVector,5,1);
            
         %counterclockwise (because of the normal vector)        
         edges = [1,2,5;
                  2,3,5;
                  4,1,5;
                  4,5,3;
                  1,2,3;
                  1,4,3];
       end
        %% Create Vertices and Eges for a Cube
       function [vert,edges] = Cube3(obj,transVector)

        vert = [0,0,0;
                1,0,0;
                1,1,0;
                0,1,0;
                0,0,1;
                1,0,1;
                1,1,1
                0,1,1];
         vert = vert +repmat(transVector,8,1);
            
         %counterclockwise (because of the normal vector)        
         edges = [1,2,3;
                  1,3,4;
                  2,6,7;
                  2,7,3;
                  5,8,7;
                  5,7,6;
                  1,4,8;
                  1,8,5;
                  4,3,7;
                  4,7,8;
                  1,6,2;
                  1,5,6];
       end
       %% Just solving a Plane EQ 
       function[A,B,C,D] = Plane3(obj,P1,P2,P3)
       
           dP2 = P1-P2;
           dP3 = P1-P3;
           n = cross(dP2,dP3);%compute normal of plane
           
           A = n(1);
           B = n(2);
           C = n(3);
           D = -dot(n,P1);
         
       end
       %% Plot a plane
       function PlotPlane3(obj,A,B,C,D,P1,P2,P3)
            x = [P1(1) P2(1) P3(1)];  
            y = [P1(2) P2(2) P3(2)];
            z = [P1(3) P2(3) P3(3)];
           
           
            xLim = [min(x) max(x)];
            zLim = [min(z) max(z)];
            [X,Z] = meshgrid(xLim,zLim);
            Y = (A * X + C * Z + D)/ (-B);
            reOrder = [1 2  4 3];
            patch(X(reOrder),Y(reOrder),Z(reOrder),'r');
       end
   end   
end
