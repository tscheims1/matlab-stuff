function varargout = ui3(varargin)
% UI3 MATLAB code for ui3.fig
%      UI3, by itself, creates a new UI3 or raises the existing
%      singleton*.
%
%      H = UI3 returns the handle to a new UI3 or the handle to
%      the existing singleton*.
%
%      UI3('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in UI3.M with the given input arguments.
%
%      UI3('Property','Value',...) creates a new UI3 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ui3_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ui3_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ui3

% Last Modified by GUIDE v2.5 05-Oct-2016 20:16:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ui3_OpeningFcn, ...
                   'gui_OutputFcn',  @ui3_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before ui3 is made visible.
function ui3_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ui3 (see VARARGIN)

% Choose default command line output for ui3
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
updateAxis(handles);
% UIWAIT makes ui3 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ui3_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function m_Callback(hObject, eventdata, handles)
% hObject    handle to m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.text_m,'String',get(hObject,'Value'))
updateAxis(handles);

% --- Executes during object creation, after setting all properties.
function m_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function q_Callback(hObject, eventdata, handles)
% hObject    handle to m_static (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
set(handles.text_q,'String',get(hObject,'Value'))
updateAxis(handles);

% --- Executes during object creation, after setting all properties.
function m_static_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m_static (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function angle_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m_static (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes during object creation, after setting all properties.
function q_CreateFcn(hObject, eventdata, handles)
% hObject    handle to m_static (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function angle_Callback(hObject, eventdata, handles)
% hObject    handle to angle_static (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

set(handles.text_angle,'String',get(hObject,'Value'))
updateAxis(handles);

% --- Executes during object creation, after setting all properties.
function angle_static_CreateFcn(hObject, eventdata, handles)
% hObject    handle to angle_static (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


function updateAxis(h)
      
    figX = [1,2,3,3,2,1,1,2,3];
    figY = [1,1,1,2,2,2,3,3,3];
    
    %% Rotation around fixed Point and Reflection in P^2
    figure1 = [figX;figY];
    figure2 = HomogenousFigure(figure1); 
    image1 = HomogenousFigure(figure1);
    image2 = HomogenousFigure(figure1);
    
    image1 = image1.Reflect(h.m.Value,h.q.Value);
    image2 = image2.RotateAroundPoint(h.angle.Value,h.center_x.Value,h.center_y.Value);
    figure2 = figure2.AddPerspective(h.perspective_x.Value,h.perspective_y.Value);
    
    
    %% Graphics
    axis('square',[-8,8,-4,12]);
    axes(h.axes1);
   
    cla;
    hold on;
    
    p = PlotHelper();
    p.PlotAxis();
    
    p.PlotLine2(figure2.fig(1,1),figure2.fig(2,1),h.perspective_x.Value,0);
    p.PlotLine2(figure2.fig(1,1),figure2.fig(2,1),0,h.perspective_y.Value);
    
    p.PlotLine(h.m.Value,h.q.Value);
    p.PlotFigure(figure1,'ro');
    p.PlotFigure(image1.fig,'go');
    p.PlotFigure(image2.fig,'bo');
    p.PlotFigure(figure2.fig,'ko');
    p.PlotFigure([h.center_x.Value;h.center_y.Value],'ko');
    
    hold off;
    %% 


function center_x_Callback(hObject, eventdata, handles)
% hObject    handle to center_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of center_x as text
%        str2double(get(hObject,'String')) returns contents of center_x as a double
handles.center_x.Value = str2double(get(hObject,'String'));

% --- Executes during object creation, after setting all properties.
function center_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to center_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function center_y_Callback(hObject, eventdata, handles)
% hObject    handle to center_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of center_y as text
%        str2double(get(hObject,'String')) returns contents of center_y as a double
handles.center_y.Value = str2double(get(hObject,'String'));

% --- Executes during object creation, after setting all properties.
function center_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to center_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in update.
function update_Callback(hObject, eventdata, handles)
% hObject    handle to update (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

updateAxis(handles)



function perspective_x_Callback(hObject, eventdata, handles)
% hObject    handle to perspective_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of perspective_x as text
%        str2double(get(hObject,'String')) returns contents of perspective_x as a double
handles.perspective_x.Value = str2double(get(hObject,'String'));

% --- Executes during object creation, after setting all properties.
function perspective_x_CreateFcn(hObject, eventdata, handles)
% hObject    handle to perspective_x (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function perspective_y_Callback(hObject, eventdata, handles)
% hObject    handle to perspective_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of perspective_y as text
%        str2double(get(hObject,'String')) returns contents of perspective_y as a double
handles.perspective_y.Value = str2double(get(hObject,'String'));

% --- Executes during object creation, after setting all properties.
function perspective_y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to perspective_y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
