filename = 'TeaKettleNode.xlsx';
M = xlsread(filename);
X = M(:,1);
Y = M(:,2);
Z = M(:,3);

filename = 'TeaKettleEdge.xlsx';
K = xlsread(filename);

figure(1);
triplot(K,X,Y,'k');
hold on;

figure(2);
trimesh(K,X,Y,Z);
colormap none;

hold off;